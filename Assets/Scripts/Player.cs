﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
public class Player : NetworkBehaviour
{

    public float motionTorque;
    public float steeringAngle;
    WheelCollider[] myWheels;
    Vector2 MotionVectors; 
    bool brakes;
    // Use this for initialization
    void Start()
    {

        Debug.Log(isLocalPlayer);
        myWheels = transform.GetComponentsInChildren<WheelCollider>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!isLocalPlayer)
            return;
        //= new Vector2()
        //{
        //    x = 1,
        //    y = Input.acceleration.x
        //};

        MotionVectors = new Vector2()
        {
            x = Input.GetAxis("Vertical"),
            y = Input.GetAxis("Horizontal")
        };
        print(Input.touchCount);
        if (Input.touches.Length > 0||Input.GetKey(KeyCode.Space))

            brakes = true;
        else
            brakes = false;
        //transform.Rotate(Vector3.up * MotionVectors.y * rotationalSpeed*.5f*MotionVectors.x);
        //transform.Translate(Vector3.forward * MotionVectors.x * motionForce);
        if(Input.GetMouseButtonUp(0))
        {
           // MotionVectors = Vector2.zero;
        }

        for (int i = 0; i < myWheels.Length; i++)
        {
            myWheels[i].motorTorque = MotionVectors.x * motionTorque;
            if (brakes)
            {
                
                myWheels[i].brakeTorque = float.MaxValue;
            }
            else
                myWheels[i].brakeTorque = 0;
            if (i < 2)
                myWheels[i].steerAngle = MotionVectors.y * steeringAngle;//*1/(1+myWheels[0].attachedRigidbody.velocity.magnitude);
            Transform ChildTransform = myWheels[i].GetComponentsInChildren<Transform>()[1];
            Quaternion ChildRotation;
            Vector3 ChildPosition;
            myWheels[i].GetWorldPose(out ChildPosition, out ChildRotation);
            ChildTransform.position = ChildPosition;
            ChildTransform.rotation = ChildRotation;
        }


    }

    public void SetTorque(float value)
    {
        MotionVectors.x = value;
    }
    public void SetSteering(float value)
    {
        MotionVectors.y = value;
    }
}
