﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(Collider))]
public class CheckPoint : MonoBehaviour
{
    int totalLaps;

    Collider Col;
    public CheckPoint Next;
    public bool IsFinal;
    public GameObject highlight;
    public bool highlighted;
    public bool Highlighted
    {
        set
        {
            highlighted = value;
            highlight.SetActive(highlighted);
        }
        get
        {
            return highlighted;
        }
    }
    private void Awake()
    {
        Col = GetComponent<Collider>();
        if (highlight.GetComponent<Collider>())
            Destroy(highlight.GetComponent<Collider>());
        Col.isTrigger = true;
        Next.Highlighted = false;
        if (IsFinal == true)
        {
            Next.Highlighted = true;
        }

    }
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter(Collider other)
    {

        PositionInTrack OthersPosition = other.attachedRigidbody.gameObject.GetComponent<PositionInTrack>();
        if (OthersPosition)
        {
            if (OthersPosition.Next == this)
            {
                OthersPosition.Next = Next;
                if(this.IsFinal==true)
                if(OthersPosition.rank==1&&OthersPosition.Lap<OthersPosition.TotalLaps)
                {
                    Ranker.RankerInstance.ExcludeTheLast();
                }

            }
        }
    }

}
