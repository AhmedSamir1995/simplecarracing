﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class CameraController : MonoBehaviour {
    public Transform player;
    public Vector3 localPosition;
	// Use this for initialization
	void Start () {
        if (!GetComponentInParent<Player>().isLocalPlayer)
            gameObject.SetActive(false);
        if(!player)
        player = GetComponentInParent<Transform>();
	}
	
	// Update is called once per frame
	void Update () {
        transform.LookAt(player.position);
	}
}
