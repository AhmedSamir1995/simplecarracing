﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
public class MaterialIdentifier : MonoBehaviour {

    public Material localPlayerMaterial;
    public Material nonLocalPlayerMaterial;
    public NetworkIdentity Player;
    public List<MeshRenderer> othersToBeChanged;
    // Use this for initialization
    void Start () {
        if (Player.isLocalPlayer)
        {
            foreach(var x in othersToBeChanged)
            {
                x.sharedMaterial = localPlayerMaterial;
            }
            GetComponent<MeshRenderer>().sharedMaterial = localPlayerMaterial;
        }
        else
        {
            foreach (var x in othersToBeChanged)
            {
                x.sharedMaterial = nonLocalPlayerMaterial;
            }
            GetComponent<MeshRenderer>().sharedMaterial = nonLocalPlayerMaterial;
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
