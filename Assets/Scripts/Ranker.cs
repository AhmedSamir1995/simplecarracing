﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Ranker : NetworkBehaviour
{
    public List<PositionInTrack> Players;
    private void Start()
    {
        Players = new List<PositionInTrack>();
    }
    // Update is called once per frame
    void Update()
    {
        if (!isServer)
            return;
        SortPlayers();
        SetPlayersRanks();
    }
    public static Ranker RankerInstance;
    Ranker()
    {
        if (RankerInstance == null)
            RankerInstance = this;
    }

    public void ExcludeTheLast()
    {
        if (Players.Count == 1)
            return;
        PositionInTrack LastPlayer = Players[Players.Count - 1];
        LastPlayer.RpcLose();
    }

    void SetPlayersRanks()
    {
        for (int i = 0; i < Players.Count; i++)
        {
            Players[i].RpcSetRank(i + 1);
        }
    }

    void SortPlayers()
    {
        Players.Sort((x, y) => { return -x.Position.CompareTo(y.Position); });
    }

}
